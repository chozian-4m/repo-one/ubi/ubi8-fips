# Overview

Red Hat Universal Base Image (UBI) 8.x with the crypto-polcy set to Federal Information Processing Standard (`FIPS`).

See the Red Hat [security hardening](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening) guide for more details.
